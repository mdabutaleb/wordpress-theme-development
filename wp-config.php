<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'u`1Z^~P_*x8B+|pn;sEW~]i^ [4w8Q~S+%UY,>5n@2[]0;D<{}PODKe7;:xoHkei');
define('SECURE_AUTH_KEY',  '={#e{SBnmsz78f%1*prGp!v$}hND4eGP!*9.1 I|8d[-Xq!vxto4Y!Rr-`O{lDGo');
define('LOGGED_IN_KEY',    'q_-<c@uU>?-m%b)=8[flXHBvf}R<Klj-)u/_(U3nOgL*XPVzXx11w[8 LbK X,jm');
define('NONCE_KEY',        '3q`KpMYR*-R3q<18vVT>B-6=cU+ED{+~!oN;Rgxb(KJv_qPd:8^E0o+n6U1l8Mi7');
define('AUTH_SALT',        'r~j@De&=5_s#;7|E+xCoX:q;`+DpY&M/#u{UAP+reTY,:6}B+|2:>3 Ey:1eqWYU');
define('SECURE_AUTH_SALT', 'xMJ7QbqAA;(%<,KI<BVsW$?co%WFH=Gpjma[o~gX,ULaAT9echQ=zSX_8|o@~vd*');
define('LOGGED_IN_SALT',   '&4+Rfb8I5i=>O6:0vKm_gW+mKG/m*5AJP4Z,}DQP3?NR[3^Wx:AZBH2J>T%?|u^C');
define('NONCE_SALT',       'b9fsc9HzRXagI-w#8p=@}6;+*w7ri9s2>yhYiUQ5$Z>f20r.=N^]o2al(-s|I<gV');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
