<?php
add_theme_support('title-tag');
//add_theme_support('custom-header');

$headerimg = array(
    'default-image' => get_template_directory_uri().'/images/header.png'
);

add_theme_support('custom-header', $headerimg);
add_theme_support('custom-background');